document.getElementById('select-category').addEventListener('change', function() {
  var selectedValue = this.value;
  var rows = Array.from(document.querySelectorAll('#jobs-table tbody tr'));

  if (selectedValue == '0')
    rows.forEach(function(row) { row.style.display = ''; });
  else
    rows.forEach(function(row) {
      row.style.display = row.getAttribute('data-cat-id') == selectedValue ? '' : 'none';
    });
});