from django.db import models

# Create your models here.
class Employee(models.Model):
    name = models.CharField(max_length=32)
    surname = models.CharField(max_length=32)
    patronymic = models.CharField(max_length=32)
    age = models.SmallIntegerField()
    gender = models.CharField(max_length=1) # м/ж
    job = models.ForeignKey('Job', on_delete=models.CASCADE)

    def __str__(self):
        return f"{self.name} {self.surname} {self.patronymic}"

class Job(models.Model):
    job_name = models.CharField(max_length=255)
    category = models.ForeignKey('Category', on_delete=models.CASCADE)

    def __str__(self):
        return f"{self.job_name} ({self.category.category_name})"

class Category(models.Model):
    category_name = models.CharField(max_length=255)

    def __str__(self):
        return self.category_name