from django.shortcuts import redirect, render
from django.contrib import messages
from django.db import connection

# Главная страница
def index(request):
    return __render_index(request)


# Employees

# Карточка сотрудника
def employee(request, employee_id):
    cursor = connection.cursor()
    cursor.execute(f'SELECT * FROM "WorkersHandbook_employee" WHERE id = {employee_id}')
    employee = cursor.fetchone()
    jobs = __get_all_jobs(cursor)
    return render(request, "employee.html", { 'data': { 'employee' : employee, 'jobs': jobs } })

# Обновление сотрудника
def update_employee(request):
    # Валидация на корректный ID
    if (not request.POST["employee_id"].isdigit()):
        messages.error(request, 'Сотрудник не существует.')
        return redirect(request.META['HTTP_REFERER'])
    # Валидация на корректное ФИО
    full_name = request.POST["employee_full_name"].split()
    if (len(full_name) != 3):
        messages.error(request, 'ФИО должно состоять минимум из трёх слов.')
        return redirect(request.META['HTTP_REFERER'])
    # Валидация на корректность остальных данных
    if (__invalid_employee(request.POST)):
        messages.error(request, 'Неверные данные')
        return redirect(request.META['HTTP_REFERER'])

    __create_or_update_employee(connection.cursor(), {
        'name': full_name[0], 'surname': full_name[1], 'patronymic': full_name[2],
        'age': int(request.POST["employee_age"]), 'gender': request.POST["employee_gender"],
        'job_id': request.POST["employee_job_id"], 'id': int(request.POST["employee_id"])
    })
    return redirect(request.META['HTTP_REFERER'])

# Создать нового сотрудника
def create_employee(request):
    # Валидация на корректное ФИО
    full_name = request.POST["employee_full_name"].split()
    if (len(full_name) != 3):
        messages.error(request, 'ФИО должно состоять минимум из трёх слов.')
        return redirect(request.META['HTTP_REFERER'])

    # Валидация на корректность остальных данных
    if (__invalid_employee(request.POST)):
        messages.error(request, 'Неверные данные')
        return redirect(request.META['HTTP_REFERER'])

    __create_or_update_employee(connection.cursor(), {
        'name': full_name[0], 'surname': full_name[1], 'patronymic': full_name[2],
        'age': int(request.POST["employee_age"]), 'gender': request.POST["employee_gender"],
        'job_id': request.POST["employee_job_id"]
    })
    return redirect(request.META['HTTP_REFERER'])

# Удалить сотрудника
def delete_employee(request):
    __delete_employee(connection.cursor(), request.POST['employee_id'])
    return redirect(request.META['HTTP_REFERER'])


# Jobs

# Карточка должности
def job(request, job_id):
    cursor = connection.cursor()
    cursor.execute(f'SELECT * FROM "WorkersHandbook_job" WHERE id = {job_id}')
    job = cursor.fetchone()
    return render(request, "job.html", { 'data': { 'job': job, 'cats': __get_all_categories(cursor) } })

# Обновить должность
def update_job(request):
    # Валидация на корректный ID
    if (not request.POST["job_id"].isdigit()):
        messages.error(request, 'Должность не существует.')
        return redirect(request.META['HTTP_REFERER'])
    # Валидация на корректную категорию
    if (not __category_exists(request.POST["job_category_id"])):
        messages.error(request, 'Категория не существует.')
        return redirect(request.META['HTTP_REFERER'])
        
    __create_or_update_job(connection.cursor(), request.POST)
    return redirect(request.META['HTTP_REFERER'])

# Создать должность
def create_job(request):
    __create_or_update_job(connection.cursor(), request.POST)
    return redirect(request.META['HTTP_REFERER'])

# Удалить должность
def delete_job(request):
    __delete_job(connection.cursor(), request.POST['job_id'])
    return redirect(request.META['HTTP_REFERER'])


# Приватные методы

def __render_index(request):
    cursor = connection.cursor()
    data = {
        'employees': __get_all_employees(cursor),
        'jobs': __get_all_jobs(cursor),
        'categories': __get_all_categories(cursor),
    }
    return render(request, 'index.html', { 'data': data })

def __invalid_employee(params):
    age = int(params["employee_age"])
    gender = params["employee_gender"]
    job_id = params["employee_job_id"]

    wrong_age = not isinstance(age, int)
    wrong_gender = wrong_age or (gender != 'м' and gender != 'ж')
    return wrong_gender or not __job_exist(job_id)


# INSERT, UPDATE, DELETE

def __create_or_update_employee(cursor, params):
    if params.get('id'):
        cursor.execute(f'''UPDATE "WorkersHandbook_employee"
                           SET (name, surname, patronymic, age, gender, job_id) =
                           ('{params['name']}', '{params['surname']}', '{params['patronymic']}', '{params['age']}', '{params['gender']}', '{params['job_id']}')
                           WHERE id = {params["id"]}''')
    else:
        cursor.execute(f'''INSERT INTO "WorkersHandbook_employee"
                           (name, surname, patronymic, age, gender, job_id)
                           VALUES
                           ('{params['name']}', '{params['surname']}', '{params['patronymic']}', '{params['age']}', '{params['gender']}', '{params['job_id']}')''')

def __delete_employee(cursor, employee_id):
    cursor.execute(f'DELETE FROM "WorkersHandbook_employee" as emp WHERE emp.id = {employee_id}')


def __create_or_update_job(cursor, params):
    if (params.get('job_id')):
        cursor.execute(f'''UPDATE "WorkersHandbook_job" SET (job_name, category_id) =
                           ('{params['job_name']}', '{params['job_category_id']}')
                           WHERE id = {params["job_id"]}''')
    else:
        cursor.execute(f'''INSERT INTO "WorkersHandbook_job" (job_name, category_id)
                           VALUES ('{params['job_name']}', '{params['job_category_id']}')''')

def __delete_job(cursor, job_id):
    cursor.execute(f'DELETE FROM "WorkersHandbook_job" as job WHERE job.id = {job_id}')


# SELECT, EXISTS

def __get_all_employees(cursor):
    # Получить всю информацию о сотрудниках и их должности
    cursor.execute('''SELECT emp.id as "№", CONCAT(name, ' ', surname, ' ', patronymic) as ФИО,
                             age as Возраст, gender as Пол, job_name as Должность, category_name
                      FROM "WorkersHandbook_employee" as emp
                      INNER JOIN "WorkersHandbook_job" as job ON job_id = job.id
                      INNER JOIN "WorkersHandbook_category" as cat ON job.category_id = cat.id''')
    # headers - заголовки таблицы, values - полученные значения
    return { 'headers' : cursor.description, 'values': cursor.fetchall() }

def __get_all_jobs(cursor):
    # Получить все должности и их категории
    cursor.execute('''SELECT job.id as "№", job_name as Должность,
                             category_name as Категория, cat.id
                      FROM "WorkersHandbook_job" as job
                      INNER JOIN "WorkersHandbook_category" as cat
                      ON job.category_id = cat.id''')
    # headers - заголовки таблицы, values - полученные значения
    return { 'headers' : cursor.description, 'values': cursor.fetchall() }

def __get_all_categories(cursor):
    # Получить все должности и их категории
    cursor.execute('SELECT id, category_name FROM "WorkersHandbook_category"')
    return cursor.fetchall()

def __category_exists(cat_id):
    cursor = connection.cursor()
    cursor.execute(f'SELECT EXISTS (SELECT 1 FROM "WorkersHandbook_category" WHERE id = {cat_id})')
    return cursor.fetchone()[0]

def __job_exist(job_id):
    cursor = connection.cursor()
    cursor.execute(f'SELECT EXISTS (SELECT 1 FROM "WorkersHandbook_job" WHERE id = {job_id})')
    return cursor.fetchone()[0]
