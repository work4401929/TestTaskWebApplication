from django.conf.urls import url
from . import views

urlpatterns = [
    # /handbook/
    url(r'^$', views.index, name='index'),

    #/handbook/job/id
    url(r'^job/(?P<job_id>[0-9]+)/', views.job, name='job'),
    #/handbook/job/update
    url(r'^job/update/', views.update_job, name='update_job'),
    #/handbook/job/create/
    url(r'^job/create/', views.create_job, name='create_job'),
    #/handbook/job/delete/
    url(r'^job/delete/', views.delete_job, name='delete_job'),

    #/handbook/employee/id
    url(r'^employee/(?P<employee_id>[0-9]+)/', views.employee, name='employee'),
    #/handbook/employee/update
    url(r'^employee/update/', views.update_employee, name='update_employee'),
    #/handbook/employee/create
    url(r'^employee/create/', views.create_employee, name='create_employee', ),
    #/handbook/employee/delete
    url(r'^employee/delete/', views.delete_employee, name='delete_employee'),
]