from django.apps import AppConfig


class WorkershandbookConfig(AppConfig):
    name = 'WorkersHandbook'
