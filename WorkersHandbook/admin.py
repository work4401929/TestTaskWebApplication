from django.contrib import admin
from .models import Category, Job, Employee

# Register your models here.
admin.site.register({ Category, Job, Employee })