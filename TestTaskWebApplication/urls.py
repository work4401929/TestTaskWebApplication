from django.shortcuts import redirect
from django.conf.urls import include, url
from django.contrib import admin

urlpatterns = [
    url(r'^$', lambda request: redirect('/handbook/')),
    url(r'^handbook/', include('WorkersHandbook.urls')),

    url(r'^admin/', admin.site.urls),
]
